# Extracting Event Logs for Process Mining from Data Stored on the Blockchain

> WARNING
> You are on branch master, which is for the pre-release version of the paper. Follow [**this link**](https://gitlab.com/MacOS/extracting-event-logs-from-process-data-on-the-blockchain/tree/paper) to get to the
> paper ready version of this repository.

# Paper

## BibTex Entry
Please cite this research if you use it in any form with the following BibTex key.
By doing so, you not only support the research but also the effort of making this repository public.
```
@InProceedings{Muehlberger.etal/SPBP2019:ExtractingEventLogsfromBlockchain,
    author    = {M{\"u}hlberger, Roman and Bachhofner, Stefan and Di Ciccio, Claudio and Garc{\’i}a-Ba{\~n}uelos, Luciano and L{\’o}pez-Pintado, Orlenys},
    title     = {Extracting Event Logs for Process Mining from Data Stored on the Blockchain},
    booktitle = {Business Process Management Workshops - {BPM} 2019 International Workshops, Vienna, Austria, September 2, 2019},
    year      = {2019},
    editor    = {Dijkman, Remco and Di Francescomarino, Chiara, and Zdun, Uwe},
    series    = {Lecture Notes in Business Information Processing},
    publisher = {Springer},
    year      = {2019},
    keywords  = {Ethereum; Process Discovery; Process Monitoring; Process Conformance}
}
```

## Resources
Paper (preprint): [https://easychair.org/publications/preprint/cW8l](https://easychair.org/publications/preprint/cW8l)

Presentation slides: [https://easychair.org/smart-slide/slide/zBS5](https://easychair.org/smart-slide/slide/zBS5)

## Abstract
The integration of business process management with blockchains across
organisational borders provides a means to establish transparency of execution and
auditing capabilities. To enable process analytics, though, non-trivial extraction and
transformation tasks are necessary on the raw data stored in the ledger. In this paper,
we describe our approach to retrieve process data from an Ethereum blockchain ledger
and subsequently convert those data into an event log formatted according to the IEEE
Extensible Event Stream (XES) standard. We show a proof-of-concept software artefact
and its application on a data set produced by the smart contracts of a process execution
engine stored on the public Ethereum blockchain network.

## Authors
[Roman Mühlberger](https://www.xing.com/profile/Roman_Muehlberger)

[Stefan Bachhofner](https://scholar.google.com/citations?hl=de&user=-WZ0YuUAAAAJ)

[Claudio Di Ciccio](https://scholar.google.at/citations?user=OBwQoWsAAAAJ&hl=en&oi=ao)

[Luciano García-Bañuelos](https://scholar.google.com/citations?user=ly9UiYMAAAAJ&hl=de)

[Orlenys López-Pintado](https://scholar.google.com/citations?user=nP1fb3sAAAAJ&hl=en)


# Explanation of Files
[**incident_management_process.ipynb**](https://gitlab.com/MacOS/extracting-event-logs-from-process-data-on-the-blockchain/blob/master/incident_management_process.ipynb)

The software that was written to scrape the Ethereum public blockchain in a jupyter notebook.

[**incident_management_process.xes**](https://gitlab.com/MacOS/extracting-event-logs-from-process-data-on-the-blockchain/blob/master/incident_management_process.xes)

The log file in eXtensible Event Stream (XES) that was generated from data stored on the blockchain. 